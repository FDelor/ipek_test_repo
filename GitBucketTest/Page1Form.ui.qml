import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {
    property alias textField1: textField1
    property alias button1: button1
    property alias textArea: textArea

    RowLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        anchors.top: parent.top

        TextField {
            id: textField1
            text: "enter your name"
            placeholderText: qsTr("Text Field")
        }

        Button {
            id: button1
            text: qsTr("Press Me")
        }
    }

    GridLayout {
        id: gridLayout
        x: 134
        y: 142
        width: 395
        height: 247
    }

    TextArea {
        id: textArea
        x: 147
        y: 163
        width: 160
        height: 56
        font.pixelSize: spinBox.value
    }

    Switch {
        id: switch1
        x: 328
        y: 163
        text: qsTr("Switch")
    }

    SpinBox {
        id: spinBox
        x: 167
        y: 267
        value: 6
        property int property0: 7
    }
}
